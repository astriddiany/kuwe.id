package com.kuwe.project.storelocation.controller;

import com.kuwe.project.storelocation.core.Location;
import com.kuwe.project.storelocation.core.Restaurant;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

@Controller
public class StoreLocationController {


    @GetMapping("/find-form")
    public String findstore(Model model) {
        model.addAttribute("location", new Location());
        return "findStoreForm";
    }

    @PostMapping("/find-form")
    public String findStoreFormSubmit(@ModelAttribute Location location) {
        return "storeList";
    }
}
