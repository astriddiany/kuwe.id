package com.kuwe.project.observer.service;

import com.kuwe.project.observer.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ObserverService {
    private Game game;

    @Autowired
    public ObserverService() {
        this.game = new Game();
    }

    public Game getGameResult() {
        return this.game;
    }
}