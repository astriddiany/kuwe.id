package com.kuwe.project.observer.controller;

import com.kuwe.project.observer.core.Game;
import com.kuwe.project.observer.service.*;
import java.util.HashMap;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ObserverController {

    private final ObserverService observerService;

    private Game game;

    public ObserverController(ObserverService observerService) {
        this.observerService = observerService;
    }

    /**
     * toppings.
     */
    @SuppressWarnings({"checkstyle:WhitespaceAround","checkstyle:VariableDeclarationUsageDistance"})
    @RequestMapping(path = "/getResult", method = RequestMethod.POST)
    public String addTopping(
            @RequestParam(value = "namakuwe") String namakuwe,
            @RequestParam(value = "jeniskuwe") String jeniskuwe,
            @RequestParam(value = "topping1", required = false) String topping1,
            @RequestParam(value = "topping1Jumlah", required = false) Integer topping1Jumlah,
            @RequestParam(value = "topping2", required = false) String topping2,
            @RequestParam(value = "topping2Jumlah", required = false) Integer topping2Jumlah,
            @RequestParam(value = "topping3", required = false) String topping3,
            @RequestParam(value = "topping3Jumlah", required = false) Integer topping3Jumlah,
            @RequestParam(value = "cakePhoto") String cakePhoto,
            Model model) {
        HashMap<String, Integer> toppings = new HashMap<String, Integer>();
        toppings.put(topping1, topping1Jumlah);
        if (toppings.containsKey(topping2)) {
            toppings.put(topping2, topping2Jumlah + toppings.get(topping2));
        } else {
            toppings.put(topping2, topping2Jumlah);
        }
        if (toppings.containsKey(topping3)) {
            toppings.put(topping3, topping3Jumlah + toppings.get(topping3));
        } else {
            toppings.put(topping3, topping3Jumlah);
        }
        game = new Game();

        game.addIngredient(topping1);
        game.addIngredient(topping2);
        game.addIngredient(topping3);
        game.addIngredient("Flour");
        game.setCakePhoto(cakePhoto);

        model.addAttribute("result", game);
        return "resultPage";
    }
}
