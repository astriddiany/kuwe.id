package com.kuwe.project.observer.core;

public class NutritionInfo extends Info {
    /**
     * recipe info.
     */
    public NutritionInfo(Game kue) {
        this.api = "Nutritionix";
        this.game = kue;
    }

    @Override
    public void update() {
        this.ingredients = game.getIngredientsList();
    }

}

