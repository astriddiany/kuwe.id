package com.kuwe.project.observer.core;

public class RecipeInfo extends Info {
    /**
     * recipe info.
     */
    public RecipeInfo(Game kue) {
        this.api = "Spoonacular";
        this.game = kue;
    }

    @Override
    public void update() {
        this.ingredients = game.getIngredientsList();
    }

}

