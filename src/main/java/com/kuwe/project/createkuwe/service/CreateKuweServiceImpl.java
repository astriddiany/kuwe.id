package com.kuwe.project.createkuwe.service;

import com.kuwe.project.createkuwe.core.kuwe.Coklat;
import com.kuwe.project.createkuwe.core.kuwe.Vanilla;
import java.util.ArrayList;

public class CreateKuweServiceImpl implements CreateKuweService {

    private ArrayList<com.kuwe.project.createkuwe.core.kuwe.Kuwe> kuwe;
    private com.kuwe.project.createkuwe.repository.CreateKuweRepository createKuweRepository;

    public CreateKuweServiceImpl(ArrayList<com.kuwe.project.createkuwe.core.kuwe.Kuwe> kuwe) {
        this.kuwe = kuwe;
        createKuwe();
    }
    /**
     * Create.
     */

    public void createKuwe() {
        com.kuwe.project.createkuwe.core.kuwe.Kuwe coklat = new Coklat();
        com.kuwe.project.createkuwe.core.kuwe.Kuwe vanilla = new Vanilla();
        kuwe.add(coklat);
        kuwe.add(vanilla);
    }

    @Override
    public void addToppings(String topping, String kuweName) {
        createKuweRepository.addToppingToKuwe(kuwe, kuweName, topping);
    }

    @Override
    public Iterable<com.kuwe.project.createkuwe.core.kuwe.Kuwe> getAllKuwe() {
        return kuwe;
    }

}
