package com.kuwe.project.createkuwe.core.kuwe;

import java.util.ArrayList;

public class Vanilla extends Kuwe {
    /**
     * Vanilla.
     */
    public Vanilla() {
        this.namaKuwe = "Kuwe Vanilla";
        this.kuweDescription = "Kuwe vanilla maknyus";
        this.toppings = new ArrayList<>();
    }
}
