package com.kuwe.project.createkuwe.core.toppings;

import com.kuwe.project.createkuwe.core.kuwe.Kuwe;

public enum Topping {
    CHOC_TOPPING,
    STRW_TOPPING;

    /**
     * topping.
     */
    public Kuwe addTopping(Kuwe kuwe) {

        if (this == Topping.CHOC_TOPPING) {
            kuwe = new Chocolate(kuwe);
        } else if (this == Topping.STRW_TOPPING) {
            kuwe = new Strawberry(kuwe);
        }

        return kuwe;
    }
}
