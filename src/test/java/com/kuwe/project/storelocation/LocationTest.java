package com.kuwe.project.storelocation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.kuwe.project.storelocation.core.Location;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class LocationTest {
    private Location location;

    @BeforeEach
    public void setUp() {
        location = new Location();
    }

    @Test
    public void testConstructorWithArgs() {
        Location newLoc = new Location(1, 2);
        assertEquals(newLoc.getLatitude(), 1);
        assertEquals(newLoc.getLongitude(), 2);
    }

    @Test
    public void testSetLatitude() {
        double latitude = 123;
        location.setLatitude(latitude);

        assertEquals(latitude, location.getLatitude());
    }

    @Test
    public void testSetLongitude() {
        double longitude = -123;
        location.setLongitude(longitude);

        assertEquals(longitude, location.getLongitude());
    }


}
