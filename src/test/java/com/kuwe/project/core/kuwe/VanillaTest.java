package com.kuwe.project.core.kuwe;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.kuwe.project.createkuwe.core.kuwe.Kuwe;
import com.kuwe.project.createkuwe.core.kuwe.Vanilla;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class VanillaTest {

    private Kuwe kuwe;

    @BeforeEach
    public void setUp() {
        kuwe = new Vanilla();
    }

    @Test
    public void testMethodGetKuweName() {
        assertEquals("Kuwe Vanilla", kuwe.getNamaKuwe());
    }

    @Test
    public void testMethodGetKuweDescription() {
        assertEquals("Kuwe vanilla maknyus", kuwe.getKuweDescription());
    }

    @Test
    public void testMethodGetKuweTopping() {
        assertTrue(kuwe.getToppings().isEmpty());
    }
}
