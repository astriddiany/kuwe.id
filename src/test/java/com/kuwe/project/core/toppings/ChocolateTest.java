package com.kuwe.project.core.toppings;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.kuwe.project.createkuwe.core.kuwe.Coklat;
import com.kuwe.project.createkuwe.core.toppings.Chocolate;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


@SuppressWarnings("checkstyle:TypeName")
public class ChocolateTest {

    private Chocolate chocolate;

    @BeforeEach
    public void setUp() {
        chocolate = new Chocolate(new Coklat());
    }

    @Test
    public void testMethodGetKuweName() {
        String name = chocolate.getNamaKuwe();
        assertEquals("Kuwe Coklat", name);
    }

    @Test
    public void testGetMethodKuweDescription() {
        String desc = chocolate.getKuweDescription();
        assertEquals("Kuwe coklat enak bat mantap!!", desc);
    }

    @Test
    public void testMethodGetKuweToppings() {
        ArrayList<String> toppings = chocolate.getToppings();
        assertTrue(toppings.contains("Chocolate"));
    }

}
