package com.kuwe.project.createkuwe;

import static junit.framework.TestCase.assertTrue;

import com.kuwe.project.createkuwe.core.kuwe.Coklat;
import com.kuwe.project.createkuwe.core.kuwe.Kuwe;
import com.kuwe.project.createkuwe.core.kuwe.KuweProducer;
import com.kuwe.project.createkuwe.core.kuwe.Vanilla;
import com.kuwe.project.createkuwe.core.toppings.Topping;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CreateKuweRepositoryTest {

    ArrayList<Kuwe> kuwes = new ArrayList<>();
    Kuwe kuwe;


    @BeforeEach
    public void setUp() {
        kuwes.add(new Coklat());
        kuwes.add(new Vanilla());
    }

    @Test
    public void testMethodSetupKuwes() {

        for (Kuwe kuwe : kuwes) {

            if (kuwe.getNamaKuwe().equals("Kuwe Coklat")) {

                Kuwe coklat;
                coklat = KuweProducer.KUWE_COKLAT.createKuwe();
                coklat = Topping.CHOC_TOPPING.addTopping(coklat);
                int index = kuwes.indexOf(kuwe);
                kuwes.set(index, coklat);
                assertTrue(coklat.getToppings().contains("Chocolate"));
            } else if (kuwe.getNamaKuwe().equals("Kuwe Vanilla")) {

                Kuwe vanilaStroberi;
                vanilaStroberi = KuweProducer.KUWE_COKLAT.createKuwe();
                vanilaStroberi = Topping.STRW_TOPPING.addTopping(vanilaStroberi);
                int index = kuwes.indexOf(kuwe);
                kuwes.set(index, vanilaStroberi);
                assertTrue(vanilaStroberi.getToppings().contains("Strawberry"));
            }
        }
    }
}
