package com.kuwe.project.createkuwe;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.kuwe.project.createkuwe.controller.CreateKuweController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;


@WebMvcTest(controllers = CreateKuweController.class)
public class CreateKuweControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenHomeUrlIsAccessedItShouldContainCorrectPath() throws Exception {

        mockMvc.perform(get("/create"))
                .andExpect(status().isOk());
    }
}
