package com.kuwe.project.createkuwe;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

import com.kuwe.project.createkuwe.core.kuwe.Coklat;
import com.kuwe.project.createkuwe.core.kuwe.Kuwe;
import com.kuwe.project.createkuwe.core.kuwe.Vanilla;
import com.kuwe.project.createkuwe.service.CreateKuweServiceImpl;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CreateKuweServiceTest {

    ArrayList<Kuwe> kuwes = new ArrayList<>();
    com.kuwe.project.createkuwe.repository.CreateKuweRepository createKuweRepository;
    CreateKuweServiceImpl createKuweService = new CreateKuweServiceImpl(kuwes);

    /**
     * setUp.
     */
    @BeforeEach
    public void setUp() {
        kuwes.add(new Coklat());
        kuwes.add(new Vanilla());
        createKuweService.createKuwe();
    }

    @Test
    public void getAllKuweTest() {
        assertEquals(createKuweService.getAllKuwe(), kuwes);
    }

    @Test
    public void addToppingsTest() {
        String toppings = "Chocolate";
        String namaKuwe = "Kuwe Coklat";
        //        createKuweRepository.addToppingToKuwe(kuwes, "Chocolate", "Kuwe Coklat");
        //        for(Kuwe kuwe: kuwes){
        //            if (kuwe.getNamaKuwe().equals("Kuwe Coklat")){
        //                assertTrue(kuwe.getToppings().contains("Chocolate"));
        //            }
        //        }
    }

}
