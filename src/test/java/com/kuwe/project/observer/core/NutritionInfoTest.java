package com.kuwe.project.observer.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class NutritionInfoTest {

    private NutritionInfo infoNutrition;
    private Game kue;

    @BeforeEach
    public void setUp() {
        kue = new Game();
        infoNutrition = kue.getNutritionInfo();
    }

    @Test
    public void usesNutritionixapi() {
        assertEquals("Nutritionix", infoNutrition.getApi());
    }

    @Test
    public void nutritionUpdateAfterBroadcast() {
        kue.addIngredient("Chocolate");
        kue.addIngredient("Vanilla");
        assertEquals(kue.getIngredientsList().size(), infoNutrition.getIngredients().size());
    }
}