package com.kuwe.project.observer.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kuwe.project.observer.core.Game;
import com.kuwe.project.observer.service.ObserverService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;


@WebMvcTest(controllers = ObserverController.class)
public class ObserverControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private Game game;

    @MockBean
    private ObserverService observerService;


    @Test
    public void resultPageIsAccessedWithRepetitiveForm() throws Exception {
        LinkedMultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();
        requestParams.add("namakuwe", "Choco Cookies");
        requestParams.add("jeniskuwe", "Cookies");
        requestParams.add("topping1", "Chocolate");
        requestParams.add("topping1Jumlah", "2");
        requestParams.add("topping2", "Chocolate");
        requestParams.add("topping2Jumlah", "2");
        requestParams.add("topping3", "Chocolate");
        requestParams.add("topping3Jumlah", "2");
        requestParams.add("cakePhoto", "/css/kuwe/kuwevanillavanilla.png");

        assertEquals(requestParams.get("topping1"), requestParams.get("topping2"));
        assertEquals(requestParams.get("topping1"), requestParams.get("topping3"));

        mockMvc.perform(post("/getResult").params(requestParams))
                .andExpect(status().isOk())
                .andExpect(view().name("resultPage"))
                .andExpect(model().attributeExists("result"));
    }

    @Test
    public void resultPageIsAccessedWithUniqueForm() throws Exception {
        LinkedMultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();
        requestParams.add("namakuwe", "Rainbow Cookies");
        requestParams.add("jeniskuwe", "Cookies");
        requestParams.add("topping1", "Blueberry");
        requestParams.add("topping1Jumlah", "1");
        requestParams.add("topping2", "Strawberry");
        requestParams.add("topping2Jumlah", "2");
        requestParams.add("topping3", "Chocolate");
        requestParams.add("topping3Jumlah", "1");
        requestParams.add("cakePhoto", "/css/kuwe/kuwevanillavanilla.png");

        assertNotEquals(requestParams.get("topping1"), requestParams.get("topping2"));
        assertNotEquals(requestParams.get("topping1"), requestParams.get("topping3"));

        mockMvc.perform(post("/getResult").params(requestParams))
                .andExpect(status().isOk())
                .andExpect(view().name("resultPage"))
                .andExpect(model().attributeExists("result"));
    }
}