package com.kuwe.project.observer.service;

import static org.junit.jupiter.api.Assertions.*;

import com.kuwe.project.observer.core.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class ObserverServiceTest {

    private ObserverService serviceDummy;
    private Game gameDummy;

    @BeforeEach
    public void setUp() {
        this.serviceDummy = new ObserverService();
    }

    @Test
    public void checkGameResult() {
        this.gameDummy = new Game();
        assertNotEquals(this.gameDummy, serviceDummy.getGameResult());
    }
}
