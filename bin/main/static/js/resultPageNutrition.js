$(document).ready(function () {
    var ingredients = $('#hasilkue').text();
    var query = ingredients.replace(/_/g, " ");

    $.ajax({

        url: "https://api.nutritionix.com/v1_1/search/" +
             query +
            "?results=0:20&fields=item_name,brand_name,item_id,nf_calories,nf_total_fat," +
            "nf_saturated_fat,nf_trans_fatty_acid,nf_cholesterol,nf_sodium,nf_total_carbohydrate," +
            "nf_dietary_fiber,nf_sugars,nf_protein&appId=12e80374&appKey=6eff58c29542b111e186c13811457cb0",

        dataType: "json",

        success: function (data) {
            var i = 0;

                document.getElementById('calories').innerHTML += data.hits[i].fields.nf_calories;
                document.getElementById('fatContent').innerHTML += data.hits[i].fields.nf_total_fat + "g";
                document.getElementById('saturatedFatContent').innerHTML += data.hits[i].fields.nf_saturated_fat + "g";
                document.getElementById('transFatContent').innerHTML += data.hits[i].fields.nf_trans_fatty_acid + "g";
                document.getElementById('cholesterolContent').innerHTML += data.hits[i].fields.nf_cholesterol + "mg";
                document.getElementById('sodiumContent').innerHTML += data.hits[i].fields.nf_sodium + "mg";
                document.getElementById('carbohydrateContent').innerHTML += data.hits[i].fields.nf_total_carbohydrate + "g";
                document.getElementById('fiberContent').innerHTML += data.hits[i].fields.nf_dietary_fiber + "g";
                document.getElementById('sugarContent').innerHTML += data.hits[i].fields.nf_sugars + "g";
                document.getElementById('protein').innerHTML += data.hits[i].fields.nf_protein + "g";
                document.getElementById('item_name').innerHTML += data.hits[i].fields.item_name + "g";

        },
        type: 'GET'
    });
});