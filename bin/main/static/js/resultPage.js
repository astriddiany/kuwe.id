$(document).ready(function() {
    var ingredients = $('#hasilkue').text();
    var query = ingredients.replace(/_/g, ",");
    query = query.replace(/ /g, "%20");
    var everythingElse = "&number=1&ranking=1&ignorePantry=1";
    var apiKey = "?apiKey=85ca72a33cb941cd91608cf2badd87a9&";
    var url="https://api.spoonacular.com/recipes/findByIngredients"+apiKey+"ingredients="+query+everythingElse;

    $.ajax({
        url:url,
        resType: "json",

        success: function (res) {

            recipeInfo.innerHTML +=
                "<h3>" + res[0].title + "</h3>" + "<img src='" + res[0].image + "' style='height: 40%;'>";

            $.ajax({
                url:"https://api.spoonacular.com/recipes/" + res[0].id + "/analyzedInstructions" + apiKey,
                resType: "json",

                success: function (res) {
                    recipeInfo.innerHTML +=
                        "<p>" + res[0].steps[0].step + "</p>";
                    /*
                    for (var i = 0; i < sum[0].steps.length; i++) {
                        recipeInfo.innerHTML +=
                            "<p>" + res[0].steps[i].step + "</p>";
                    }
                     */
                }
            })
        },
        type: 'GET',

        error : function(e) {
            alert("Error!");
            console.log("ERROR: ", e);
        }
    });
});
