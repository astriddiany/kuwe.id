# Kuwe.id
**Kuwe.id** adalah website simulator untuk pembuatan kue.
Disini user akan menghias sendiri kreasi kuenya lengkap dengan resep dari kue tersebut.
Ini adalah projek akhir untuk mata kuliah Advanced Programming dari mahasiswa Fakultas Ilmu Komputer Universitas Indonesia.

## Kelompok 5 Advanced Programming 
[![pipeline status](https://gitlab.com/muhammad.iqbal815/kuwe.id/badges/master/pipeline.svg)](https://gitlab.com/muhammad.iqbal815/kuwe.id/-/commits/master)
[![coverage report](https://gitlab.com/muhammad.iqbal815/kuwe.id/badges/master/coverage.svg)](https://gitlab.com/muhammad.iqbal815/kuwe.id/-/commits/master)  


### Anggota Kelompok:
- Mushaffa Huda
- Muhammad Iqbal Wijonarko
- Muhammad Zakiy Saputra
- Nurul Srianda Putri
- Fandy David Rivaldy
